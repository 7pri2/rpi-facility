#ifndef __PIN_H__
#define __PIN_H__

/**
 * @file Pin.h
 * @brief Definition of class Pin and Mode
 */

#include <wiringPi.h>
#include <mutex>

/**
 * @brief Enum that encapsulate wiringPi pins modes
 */
enum class Mode {
	none = -1,
	input = INPUT,
	output = OUTPUT
};

/**
 * @brief Class defined by a GPIO id. You can enable or disable the pin, or 
 * even read its state.
 *
 * The id of the pin is wiringPi's one. The class holds informations about
 * the current state and the mode of the pin.
 * This class can only be instanciated in the class GPIO. You're not expected 
 * to isntanciate it, only to use it with the class GPIO.
 */
class Pin {
friend class GPIO;
public:

	/**
	 * @brief enables the pin, encapsulation of wiringPi's `digitalWrite(pin, HIGH)`
	 */
    void enable();

	/**
	 * @brief disables the pin, encapsulation of wiringPi's `digitalWrite(pin, LOW)`
	 */
    void disable();

	/**
	 * @brief set pin to enabled of disabled
	 * @param state we want to set to the pin
	 */
    void set_enabled(bool enabled);

	/**
	 * @brief check if pin is enabled
	 * @return true if the pin is enabled, false otherwise
	 * 
	 * If pin is in input mode, encapsulation of wiringPi's `digitalRead(pin)`.
	 * In output mode, returns the last state that has been set.
	 */
    bool is_enabled();

	/**
	 * @brief check if pin is disabled
	 * @return true if the pin is disabled, false otherwise
	 * 
	 * If pin is in input mode, encapsulation of wiringPi's `!digitalRead(pin)`.
	 * In output mode, returns the last state that has been set.
	 */
    bool is_disabled();

	/**
	 * @brief Change the state of the pin
	 */
	void switch_state();

	/**
	 * @brief sets the mode for the pin (see documentation of Mode)
	 */
    void set_mode(Mode mode);

	/**
	 * @brief returns the mode of the pin
	 * @return The mode of the pin (see Mode documentation)
	 */
	Mode get_mode();

private:
    Pin(int wiringpi_id);
    ~Pin();

    int wiringpi_id;
    bool enabled;
    Mode mode;

	mutable std::mutex lock;
};

#endif /* end of include guard: __PIN_H__ */
