#ifndef __STEPPER_MOTOR_H__
#define __STEPPER_MOTOR_H__

#include "GPIO.h"
#include "Pin.h"
#include "directions.h"

class Stepper_motor {
public:
    Stepper_motor(Pin* pin1, Pin* pin2, Pin* pin3, Pin* pin4);
    ~Stepper_motor();

	void rotate_for(Direction dir, int duration_ms);
	void rotate_to(Direction dir, int angle);
	void rotate_cycle(Direction dir);

	void stop();

	/* maximum RPM :9.765625 */
	void set_speed(float rpm);
	float get_speed();

private:
	Pin* motor_pins[4];
	int speed;

	const int steps_per_round = 2048;
	const int cw_steps[4] = {0x8, 0x4, 0x2, 0x1};
	const int ccw_steps[4] = {0x1, 0x2, 0x4, 0x8};
};

#endif /* end of include guard: __STEPPER_MOTOR_H__ */
