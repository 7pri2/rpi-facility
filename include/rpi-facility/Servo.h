#ifndef __SERVO_H__
#define __SERVO_H__

/**
 * @file Servo.h
 * @brief Declaration of class Servo
 */

#include "GPIO.h"
#include "Pin.h"

/**
 * @brief Class made to simply control a servo motor
 */
class Servo {
public:
	/**
	 * @brief Servo constructor
	 * @param pin Pin wired to the servo motor
	 */
	Servo(Pin* pin);
	~Servo();

	/**
	 * @brief Makes the servo rotate to the angle
	 * @param angle Angle to reach by the servo
	 * 
	 * The method is blocking, and its duration is proportional to the 
	 * distance to travel. The duration is calculated with the angular speed
	 * (2 = 120ms/60deg) and the difference between the desired angle and the 
	 * previous angle. At initialization, the angle is not known, so the 
	 * rotation will take the maximum time (360ms)
	 */
	void set_angle(float angle);

	/**
	 * @brief Get the current angle
	 * @return Current angle of the servo
	 */
	float get_angle();

private:
	Pin* pin;
	float last_angle;
	const float angular_speed = 2;
};

#endif /* end of include guard: __SERVO_H__ */
