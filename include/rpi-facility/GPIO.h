#ifndef __GPIO_H__
#define __GPIO_H__

/**
 * @file GPIO.h
 * @brief Definition of GPIOs
 */

class Pin;

/**
 * @brief Class that holds the GPIOs
 */
class GPIO {
public:
	/**
	 * @brief Constructs the GPIOs and initialize wiringPi
	 */
	GPIO();
	~GPIO();

	/**
	 * @brief list of BCM GPIO
	 */
	Pin	*gpio2,
		*gpio3,
		*gpio4,
		*gpio14,
		*gpio15,
		*gpio17,
		*gpio18,
		*gpio27,
		*gpio22,
		*gpio23,
		*gpio24,
		*gpio10,
		*gpio9,
		*gpio25,
		*gpio11,
		*gpio8,
		*gpio7,
		*gpio0,
		*gpio1,
		*gpio5,
		*gpio6,
		*gpio12,
		*gpio13,
		*gpio19,
		*gpio16,
		*gpio26,
		*gpio20,
		*gpio21;
};

#endif /* end of include guard: __GPIO_H__ */
