#ifndef __JOYSTICK_H__
#define __JOYSTICK_H__

#include <wiringPi.h>
#include <pcf8591.h>
#include <softPwm.h>
#include <vector>
#include "Pin.h"

class JoyStick {
public:
    JoyStick(Pin* z_pin);
    ~JoyStick();

    std::vector<int> get_position();

private:
    Pin* z_pin;
    const int address = 0x48;
    const int pinbase = 64;
    const int A0 = pinbase + 0;
    const int A1 = pinbase + 1;
    const int A2 = pinbase + 2;
    const int A3 = pinbase + 3;
};

#endif /* end of include guard: __JOYSTICK_H__ */
