#ifndef __LED_H__
#define __LED_H__

/**
 * @file LED.h
 * @brief Definition of class LED
 */

#include "GPIO.h"
#include "Pin.h"

/**
 * @brief Class made to simply use LEDs
 */

class LED {
public:
	/**
	 * @brief LED constructor
	 * @param pin GPIO pin wired to the LED
	 */
    LED(Pin *pin);
    ~LED();

	/**
	 * @brief Turns on the light
	 */
	void turn_on();

	/**
	 * @brief Turns off the light
	 */
	void turn_off();

	/**
	 * @brief Switches the state of the LED
	 */
	void switch_state();
	
	/**
	 * @brief Checks the state of the LED
	 * @return true if the LED is bright, false otherwise
	 */
	bool is_on();

	/**
	 * @brief Checks the state of the LED
	 * @return false if the LED is bright, true otherwise
	 */
	bool is_off();

private:
	Pin*	pin;
};

#endif /* end of include guard: __LED_H__ */
