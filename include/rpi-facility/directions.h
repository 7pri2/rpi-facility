#ifndef __DIRECTIONS_H__
#define __DIRECTIONS_H__

enum class Direction {
	UP,
	DOWN,
	LEFT,
	RIGHT,
	CLOCKWISE,
	COUNTERCLOCKWISE
};

#endif /* end of include guard: __DIRECTIONS_H__ */
