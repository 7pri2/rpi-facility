#include "LED.h"

LED::LED(Pin *pin) {
	this->pin = pin;
	pin->set_mode(Mode::output);
}

LED::~LED() {

}

void LED::turn_on() {
	/* The LED is bright when the pin is at LOW */
	this->pin->disable();
}

void LED::turn_off() {
	/* The LED is bright when the pin is at LOW */
	this->pin->enable();
}

void LED::switch_state() {
	this->pin->switch_state();
}

bool LED::is_on() {
	return this->pin->is_disabled();
}

bool LED::is_off() {
	return this->pin->is_enabled();
}
