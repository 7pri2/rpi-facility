#include "Servo.h"
#include <cstdlib>

Servo::Servo(Pin* pin) {
	this->pin = pin;
	this->pin->set_mode(Mode::output);
	this->last_angle = -1;
}

Servo::~Servo() {

}

void Servo::set_angle(float angle) {
	for(int i = 0; i < 5; ++i) {
		this->pin->enable();
		delayMicroseconds(500+(angle*2000/180));
		this->pin->disable();
		delayMicroseconds(19500-(angle*2000/180));
	}
	if(this->last_angle == -1) // default value
		delay(360); // maximum delay
	else
		delay(abs(this->last_angle-angle)*this->angular_speed);
	this->last_angle = angle;
}

float Servo::get_angle() {
	return this->last_angle;
}
