#include "Pin.h"

Pin::Pin(int wiringpi_id) {
    this->wiringpi_id = wiringpi_id;
    this->enabled = false;
    this->mode = Mode::none;
}

Pin::~Pin() {

}

void Pin::enable() {
	std::lock_guard<std::mutex> guard(this->lock);
    if(this->mode == Mode::output) {
		digitalWrite(this->wiringpi_id, HIGH);
		this->enabled = true;
    }
}

void Pin::disable() {
	std::lock_guard<std::mutex> guard(this->lock);
    if(this->mode == Mode::output) {
		digitalWrite(this->wiringpi_id, LOW);
		this->enabled = false;
    }
}

void Pin::set_enabled(bool enabled) {
    enabled ? this->enable() : this->disable();
}

void Pin::switch_state() {
    this->is_enabled() ? this->disable() : this->enable();
}

bool Pin::is_enabled() {
	std::lock_guard<std::mutex> guard(this->lock);
    if(this->mode == Mode::input)
        return this->enabled;
    else if(this->mode == Mode::output)
        return digitalRead(this->wiringpi_id);
	else
		return false;
}

bool Pin::is_disabled() {
    return !this->is_enabled();
}

void Pin::set_mode(Mode mode) {
	this->mode = mode;
	pinMode(this->wiringpi_id, static_cast<int>(mode));
}

Mode Pin::get_mode() {
	return this->mode;
}
