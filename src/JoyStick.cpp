#include "JoyStick.h"

JoyStick::JoyStick(Pin* z_pin) {
    this->z_pin = z_pin;
    this->z_pin->set_mode(Mode::input);
    pullUpDnControl(this->z_pin, PUD_UP);
    pcf8591Setup(this->pinbase, this->address);
}

JoyStick::~JoyStick() {

}

std::vector<int> get_position() {
    std::vector<int> res;
    // Read X
    res.push_back(analogRead(A1));
    // Read Y
    res.push_back(analogRead(A0));
    // Read Z
    res.push_back(this->z_pin->is_enabled());
    return res;
}
