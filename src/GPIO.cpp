#include "GPIO.h"

#include <wiringPi.h>
#include "Pin.h"

GPIO::GPIO() {
	wiringPiSetup();
	gpio2 = new Pin(8),
	gpio3 = new Pin(9),
	gpio4 = new Pin(7),   
	gpio14 = new Pin(15),
	gpio15 = new Pin(16),
	gpio17 = new Pin(0),
	gpio18 = new Pin(1),
	gpio27 = new Pin(2),
	gpio22 = new Pin(3),
	gpio23 = new Pin(4),
	gpio24 = new Pin(5),
	gpio10 = new Pin(12),
	gpio9 = new Pin(13),
	gpio25 = new Pin(6),
	gpio11 = new Pin(14),
	gpio8 = new Pin(10),
	gpio7 = new Pin(11),
	gpio0 = new Pin(30),
	gpio1 = new Pin(31),
	gpio5 = new Pin(21),
	gpio6 = new Pin(22),
	gpio12 = new Pin(26),
	gpio13 = new Pin(23),
	gpio19 = new Pin(24),
	gpio16 = new Pin(27),
	gpio26 = new Pin(25),
	gpio20 = new Pin(28),
	gpio21 = new Pin(29);
}

GPIO::~GPIO() {

}
