#include "Stepper_motor.h"
#include <iostream>

Stepper_motor::Stepper_motor(Pin* pin1, Pin* pin2, Pin* pin3, Pin* pin4) {
	this->motor_pins[0] = pin1;
	this->motor_pins[1] = pin2;
	this->motor_pins[2] = pin3;
	this->motor_pins[3] = pin4;
	this->speed = 3;
	for(int i = 0; i < 4; ++i) {
		this->motor_pins[i]->set_mode(Mode::output);
	}
}

Stepper_motor::~Stepper_motor() {

}

void Stepper_motor::rotate_for(Direction dir, int duration_ms) {
	while(duration_ms >= 0) {
		this->rotate_cycle(dir);
		duration_ms -= this->speed;
	}
}

void Stepper_motor::rotate_to(Direction dir, int angle) {
	for(int i = 0, cycles = angle*(this->steps_per_round/4)/360; i < cycles; ++i) {
		this->rotate_cycle(dir);
	}
}

void Stepper_motor::rotate_cycle(Direction dir) {
	for(int i = 0; i < 4; ++i) {
		for(int j = 0; j < 4; ++j) {
			if(dir == Direction::CLOCKWISE) {
				this->motor_pins[j]->set_enabled(this->cw_steps[i] == (1 << j));
			} else if(dir == Direction::COUNTERCLOCKWISE) {
				this->motor_pins[j]->set_enabled(this->ccw_steps[i] == (1 << j));
			}
		}
		delay(this->speed < 3 ? 3 : this->speed);
	}
}

void Stepper_motor::stop() {
	for(int i = 0; i < 4; ++i) {
		this->motor_pins[i]->disable();
	}
}

void Stepper_motor::set_speed(float rpm) {
	this->speed = 1/rpm * 60000 / this->steps_per_round;
}

float Stepper_motor::get_speed() {
	return 60000.0 / (speed * 2048.0);
}

