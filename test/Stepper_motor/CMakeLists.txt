include_directories(../../include)

add_executable(demoStepper_motor
	demoStepper_motor.cpp)

find_library(WIRINGPI_LIBRARIES NAMES wiringPi)
target_link_libraries(demoStepper_motor LINK_PUBLIC Stepper_motor ${WIRINGPI_LIBRARIES})
