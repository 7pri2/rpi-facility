#include "rpi-facility/Stepper_motor.h"

int main(int argc, const char *argv[]) {
	GPIO gpio;
	Stepper_motor motor(gpio.gpio18,
						gpio.gpio23, 
						gpio.gpio25, 
						gpio.gpio20);
//	motor.rotate_for(Direction::CLOCKWISE, 1000);
	motor.rotate_to(Direction::CLOCKWISE, 360);
	return 0;
}
