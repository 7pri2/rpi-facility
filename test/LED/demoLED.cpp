#include "rpi-facility/LED.h"

int main() {
	GPIO gpio;
	LED l1(gpio.gpio26);
	for(int i = 0; i < 10; ++i) {
		l1.turn_on();
		delay(500);
		l1.turn_off();
		delay(500);
	}
	return 0;
}
