#include <iostream>
#include "rpi-facility/Servo.h"

int main(int argc, const char *argv[]) {
	GPIO gpio;
	Servo servo(gpio.gpio24);
	int angle = 0;
	
	while(true) {
		std::cout << "Enter angle witin range (0-180) : ";
		std::cin >> angle;
		if(angle >= 0 && angle <= 180)
			servo.set_angle(angle);
		else
			std::cout << "Beyond range" << std::endl;
	}
	return 0;
}
