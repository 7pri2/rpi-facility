#include "rpi-facility.h"
#include <thread>
#include <iostream>

void switch_states_thread(Pin* pin) {
	for(int i = 0; i < 100000; ++i) {
		std::cout << "Thread " << std::this_thread::get_id() << " switch !" << std::endl;
		pin->switch_state();
	}
}

int main(int argc, const char *argv[]) {
	GPIO gpio;
	std::thread t1(switch_states_thread, gpio.gpio1);
	std::thread t2(switch_states_thread, gpio.gpio1);
	t1.join();
	t2.join();
	return 0;
}
