cmake_minimum_required(VERSION 3.13)

project(RPI-Facility C CXX)

add_subdirectory(src)
add_subdirectory(test)
add_subdirectory(docs)
