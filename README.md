# rpi-facility

Library built upon wiringPi to use easily stepper motors, servo, LED, etc...

## Guide
### GPIO
In the programs you will write, you need to use your raspebrry pi's GPIO. In 
this lib, we abstracted the numbering of the GPIOs made by wiringPi by using 
names that fits BCM notation.

In every program, you must declare an object of type GPIO. It initializes the 
pins and wiringPi.

You can access the GPIOs by doing so:

		GPIO gpio; // creation of the object

		gpio.gpio1;	// access GPIO1

### LED
In this guide, the header `#include "rpi-facility/LED.h"` is needed.

First, you declare the GPIO

		GPIO gpio;

Then you create an object of type LED and assign it the GPIO that is wired to 
your LED (for example the GPIO26)

		LED l1(gpio.gpio26);

Finally you can use the LED (see full LED documentation)

Example:

		#include "rpi-facility/LED.h"

		int main() {
			GPIO gpio;
			LED l1(gpio.gpio26);
			for(int i = 0; i < 10; ++i) {
				l1.turn_on();
				delay(500);
				l1.turn_off();
				delay(500);
			}
			return 0;
		}

### Servo
In this guide, the header `#include "rpi-facility/Servo.h"` is needed.

First, you declare the GPIO

		GPIO gpio;

Then you create an object of type Servo and assign it the GPIO that will 
control the Servo motor (and which is wired to it). In this example we use 
GPIO24

		Servo servo(gpio.gpio24);

You can now use the Servo at will (see full Servo documentation)

Example:

		#include <iostream>
		#include "rpi-facility/Servo.h"

		int main(int argc, const char *argv[]) {
			GPIO gpio;
			Servo servo(gpio.gpio24);
			int angle;
			
			while(true) {
				std::cout << "Enter angle witin range (0-180) : ";
				std::cin >> angle;
				if(angle >= 0 && angle <= 180)
					servo.set_angle(angle);
				else
					std::cout << "Beyond range" << std::endl;
			}
			return 0;
		}


### Stepper Motor
In this guide, the header `#include "rpi-facility/Stepper_motor.h"` is needed.

First, you declare the GPIO

		GPIO gpio;

Then you create an object of type Stepper\_motor and assign it the GPIOs wired 
to the stepper motor. The order of the pins are IN1, IN2, IN3, IN4. In this 
example we use respectively GPIO18, GPIO23, GPIO25 and GPIO20

		Stepper_motor motor(gpio.gpio18,
					gpio.gpio23,
					gpio.gpio25,
					gpio.gpio20);

Now you can use the stepper motor (see full Stepper\_motor documentation)

Example

		#include "rpi-facility/Stepper_motor.h"

		int main(int argc, const char *argv[]) {
			GPIO gpio;
			Stepper_motor motor(gpio.gpio18,
						gpio.gpio23,
						gpio.gpio25,
						gpio.gpio20);
			motor.rotate_to(Direction::CLOCKWISE, 360);
			return 0;
		}


